var gulp = require("gulp");
var jshint = require("gulp-jshint");
var scsslint = require('gulp-scss-lint');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var sass = require('css-scss');
var autoprefixer = require('gulp-autoprefixer');

gulp.task("default", function() {
    console.log("Hello friends!");
});

gulp.task("lint:js", function() {
    gulp.src('src/js/**/*.js')
        .pipe(jshint());
});

gulp.task("lint:scss", function() {
  gulp.src('src/scss/**/*.scss')
    .pipe(scsslint());
});

gulp.task('style', function() {
    gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(uncss({ html: ['src/index.html' ]}))
        .pipe(cssScss())
        .pipe(autoprefixer({browsers: ['last 2 versions']}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.tmp'))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
});

gulp.task('script', function(){
    gulp.src('src/js/main.js')
    .pipe(sourcemaps.init())
    pipe(sass())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('web/css'));
});

gulp.task('serve', ['style', 'script'], function() {
    
    browserSync.init({
        server: ['.tmp', 'src']
    });
    
    gulp.watch('src/scss/**/*.scss', ['style']);
    gulp.watch('src/js/**/*.js', ['style', browserSync.reload]);
    gulp.watch('src/**/*.html', browserSync.reload);
});

gulp.task('default', ['build'])